#!/usr/bin/env bash
#
# Kali Linux ARM build-script for CuBox-i4Pro (32-bit) - Freescale based NOT the original Marvell based
# Source: https://gitlab.com/kalilinux/build-scripts/kali-arm
#
# This is a community script - you will need to generate your own image to use
# More information: https://www.kali.org/docs/arm/cubox-i4pro/
#

# Hardware model
hw_model=${hw_model:-"cubox-i4pro"}

# Architecture
architecture=${architecture:-"armhf"}

# Desktop manager (xfce, gnome, i3, kde, lxde, mate, e17 or none)
desktop=${desktop:-"xfce"}

# Load default base_image configs
source ./common.d/base_image.sh

# Network configs
basic_network
add_interface eth0

# Third stage
cat <<EOF >>"${work_dir}"/third-stage
status_stage3 'Install kernel and u-boot packages'
eatmydata apt-get install -y --no-install-recommends linux-image-armmp u-boot-menu u-boot-imx

status_stage3 'Install additional networking/hacking tools'
eatmydata apt-get install -y bridge-utils arptables ebtables #ethtool

status_stage3 'Install additional packages that KK likes to have by default'
eatmydata apt-get install -y mc lshw

status_stage3 'Enable login over serial'
echo "T0:23:respawn:/sbin/agetty -L ttymxc0 115200 vt100" >> /etc/inittab
EOF

# Run third stage
include third_stage

# Clean system
include clean_system

# For some reason the brcm firmware doesn't work properly in linux-firmware git
# so we grab the ones from OpenELEC
status "Patching Broadcom Wi-Fi firmware"
#cd ${base_dir}
#git clone https://github.com/OpenELEC/wlan-firmware
#cd wlan-firmware
#rm -rf ${work_dir}/lib/firmware/brcm
#cp -a firmware/brcm ${work_dir}/lib/firmware/

# this is now an alternative
#  mkdir -p ${work_dir}/home/kali/brcm/
#  cp -a /home/user/devel/brcmfw ${work_dir}/home/kali/brcm/
#  cp -a /home/user/devel/brcmfw-cleaned ${work_dir}/home/kali/brcm/

# this is a second alternative
  mkdir -p ${work_dir}/../brcm
  wget -P ${work_dir}/../brcm https://github.com/LibreELEC/brcmfmac_sdio-firmware/archive/refs/heads/master.zip
  unzip ${work_dir}/../brcm/master.zip -d ${work_dir}/../brcm
  find ${work_dir}/../brcm -name *4329* -exec cp -a {} ${work_dir}/lib/firmware/brcm/ \;
  find ${work_dir}/../brcm -name *4330* -exec cp -a {} ${work_dir}/lib/firmware/brcm/ \;

status "Adding other NXP firmware"
  mkdir -p ${work_dir}/../nxp
  wget -P ${work_dir}/../nxp wget https://www.nxp.com/lgfiles/NMG/MAD/YOCTO/firmware-imx-8.22.bin
  (cd ${work_dir}/../nxp/; /bin/bash firmware-imx-8.22.bin --auto-accept)
  mkdir -p ${work_dir}/lib/firmware/imx
  mkdir -p ${work_dir}/lib/firmware/imx/vpu
  mkdir -p ${work_dir}/lib/firmware/imx/sdma
  mkdir -p ${work_dir}/lib/firmware/imx/epdc
  ln -s imx/vpu ${work_dir}/lib/firmware/vpu
  ln -s imx/sdma ${work_dir}/lib/firmware/sdma
  ln -s imx/epdc ${work_dir}/lib/firmware/epdc
  cp -a ${work_dir}/../nxp/firmware-imx-8.22/firmware/vpu/*.bin ${work_dir}/lib/firmware/imx/vpu/
  cp -a ${work_dir}/../nxp/firmware-imx-8.22/firmware/sdma/*.bin ${work_dir}/lib/firmware/imx/sdma/
  cp -a ${work_dir}/../nxp/firmware-imx-8.22/firmware/epdc/*.fw ${work_dir}/lib/firmware/imx/epdc/
  cp -a ${work_dir}/../nxp/firmware-imx-8.22/firmware/epdc/epdc_ED060XH2C1.fw.nonrestricted ${work_dir}/lib/firmware/imx/epdc/epdc_ED060XH2C1.fw

cd "${repo_dir}/"

status "Make image"
# Calculate the space to create the image and create
make_image

# Create the disk partitions
status "Create the disk partitions"
parted -s "${image_dir}/${image_name}.img" mklabel msdos
parted -s -a minimal "${image_dir}/${image_name}.img" mkpart primary "$fstype" 4MiB 100%

# Set the partition variables
make_loop

# Create file systems
mkfs_partitions

# Make fstab.
make_fstab

# Create the dirs for the partitions and mount them
status "Create the dirs for the partitions and mount them"
mkdir -p "${base_dir}"/root
mount ${rootp} "${base_dir}"/root

status "Edit the extlinux.conf file to set root uuid and proper name"

if [ -f "${work_dir}/boot/extlinux/extlinux.conf" ]; then
  echo "Found ${work_dir}/boot/extlinux/extlinux.conf, patching it."
# Ensure we don't have root=/dev/sda3 in the extlinux.conf which comes from running u-boot-menu in a cross chroot
# We do this down here because we don't know the UUID until after the image is created
  sed -i -e "0,/root=.*/s//root=UUID=$root_uuid rootfstype=$fstype video=HDMI-A-1:1920x1080M@60 console=ttymxc0,115200n8 console=tty1 consoleblank=0 ro rootwait/g" ${work_dir}/boot/extlinux/extlinux.conf
# And we remove the "GNU/Linux because we don't use it
  sed -i -e "s|.*GNU/Linux Rolling|menu label Kali Linux|g" ${work_dir}/boot/extlinux/extlinux.conf
else
  echo "ERROR: Couldn't find ${work_dir}/boot/extlinux/extlinux.conf, skipping two commands."
  echo "       Even though your system won't be able to boot without creating an extlinux.conf file first,"
  echo "       at least the image will be created and you know now what to fix manually after mounting the image and before flashing to SD card."
fi

status "Set the default options in /etc/default/u-boot"

echo 'U_BOOT_MENU_LABEL="Kali Linux"' >>${work_dir}/etc/default/u-boot
echo 'U_BOOT_PARAMETERS="video=HDMI-A-1:1920x1080M@60 console=ttymxc0,115200n8 console=tty1 consoleblank=0 ro rootwait"' >>${work_dir}/etc/default/u-boot

status "Rsyncing rootfs into image file"
rsync -HPavz -q ${work_dir}/ ${base_dir}/root/
sync

status "dd U-Boot bootloader to ${loopdevice}"

if [ -f "${work_dir}/usr/lib/u-boot/mx6cuboxi/SPL" ]; then
  echo "Found U-Boot firmware in file system, copying SPL"
  dd conv=fsync,notrunc if=${work_dir}/usr/lib/u-boot/mx6cuboxi/SPL of=${loopdevice} bs=1k seek=1
else
  echo "WARNING: Downloading and including SPL from Solidrun site..."
  echo "The created image might not boot as expected, however, it should be fixable and at least an image will be created."
  mkdir -p ${work_dir}/../u-boot
  wget -P ${work_dir}/../u-boot https://solid-run-images.sos-de-fra-1.exo.io/IMX6/U-Boot/v2018.01/20230807-a5fd97a0f1/spl-imx6-sdhc.bin
  dd conv=fsync,notrunc if=${work_dir}/../u-boot/spl-imx6-sdhc.bin of=${loopdevice} bs=1k seek=1
fi

if [ -f "${work_dir}/usr/lib/u-boot/mx6cuboxi/u-boot.img" ]; then
  echo "Found U-Boot firmware in file system, copying IMG"
  dd conv=fsync,notrunc if=${work_dir}/usr/lib/u-boot/mx6cuboxi/u-boot.img of=${loopdevice} bs=1k seek=69
else
  echo "WARNING: Downloading and including U-Boot IMG from Solidrun site..."
  echo "The created image might not boot as expected, however, it should be fixable and at least an image will be created."
  mkdir -p ${work_dir}/../u-boot
  wget -P ${work_dir}/../u-boot https://solid-run-images.sos-de-fra-1.exo.io/IMX6/U-Boot/v2018.01/20230807-a5fd97a0f1/u-boot-imx6-sdhc.img
  dd conv=fsync,notrunc if=${work_dir}/../u-boot/u-boot-imx6-sdhc.img of=${loopdevice} bs=1k seek=69
fi

# Load default finish_image configs
status "Finishing everything"
include finish_image

